/**
 * Ordered Set Drag 'n' Drop handler
 */

Drupal.behaviors.orderedSetDrag = function(context) {
  for (tableId in Drupal.settings.tableDrag) {
    var table = $('#' + tableId);

    var tableDrag = Drupal.tableDrag[tableId]; // Get the tableDrag object.

    // Add a handler for when a row is swapped, update empty categories.
    tableDrag.row.prototype.onSwap = function(swappedRow) {
      var rowObject = this;
      $('tr.region-message', table).each(function() {
        // If the dragged row is in this category, but above the message row, swap it down one space.
        if ($(this).prev('tr').get(0) == rowObject.element) {
          // Prevent a recursion problem when using the keyboard to move rows up.
          if ((rowObject.method != 'keyboard' || rowObject.direction == 'down')) {
            rowObject.swap('after', this);
          }
        }
        // This category has become empty
        if ($(this).next('tr').is(':not(.draggable)') || $(this).next('tr').size() == 0) {
          $(this).removeClass('region-populated').addClass('region-empty');
        }
        // This category has become populated.
        else if ($(this).is('.region-empty')) {
          $(this).removeClass('region-empty').addClass('region-populated');
        }
      });
    };

    // Add a handler so when a row is dropped, update fields dropped into new categories.
    tableDrag.onDrop = function() {
      dragObject = this;
      if ($(dragObject.rowObject.element).prev('tr').is('.region-message')) {
        var regionRow = $(dragObject.rowObject.element).prev('tr').get(0);
        var regionName = regionRow.className.replace(/([^ ]+[ ]+)*region-([^ ]+)-message([ ]+[^ ]+)*/, '$2');
        var visibleField = $('select.item-visible', dragObject.rowObject.element);
        var weightField = $('select.item-weight', dragObject.rowObject.element);
        var oldRegionName = weightField[0].className.replace(/([^ ]+[ ]+)*item-weight-([^ ]+)([ ]+[^ ]+)*/, '$2');

        if (!visibleField.is('.item-visible-'+ regionName)) {
          visibleField.removeClass('item-visible-' + oldRegionName).addClass('item-visible-' + regionName);
          weightField.removeClass('item-weight-' + oldRegionName).addClass('item-weight-' + regionName);
          visibleField.val(regionName);
        }
      }
    };
  }
};
